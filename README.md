# shovel

管理 CI/CD 的運行腳本，主要使用 Terraform 及 Ansible

# Requirements

- Terraform > 5.0
- ansible

# 資料夾說明

- infra：機器資源的 terraform 腳本，平台為 AWS
- ansible：安裝、部屬服務的腳本
- wordpress：測試建置 wordpress 服務的設定檔

# Terraform 安裝設定

以下操作環境為 windows

## 安裝

- 安裝 windows 的套件管理工具 chocolatey，可參考[官網](https://docs.chocolatey.org/en-us/choco/setup)自行選擇適合的安裝方式
```
$ Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```
- 使用有系統管理員權限的終端，執行安裝 Terraform 的指令
```
$ choco install terraform
```

## 驗證 AWS 身分

操作資源會存取 AWS 的 API，因此事前需要設定並驗證身分，
為避免異常操作和資安問題，建議使用有限縮權限的子帳號

### AWS CLI

- 安裝 AWS CLI，參考[官網](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
- 設定本機 config，輸入帳號的 access key
```
$ aws configure
```
