terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

# EC2

resource "aws_instance" "ball-pool-test" {
  ami                     = "ami-0f403e3180720dd7e" # Amazon Linux 2023 AMI 2023.3.20240304.0 x86_64 HVM kernel-6.1
  instance_type           = "t2.micro"

  tags = {
    Name = "ball-pool-test"
    Info = "wordpress 網站"
  }
}
