
# VPC
resource "aws_vpc" "ball-pool-vpc" {
  cidr_block = "70.0.0.0/24"

  tags = {
    Name = "ball-pool-vpc"
    Info = "測試用網段"
  }
}

resource "aws_subnet" "ball-public-1" {
  vpc_id            = aws_vpc.ball-pool-vpc.id
  cidr_block        = "70.0.0.0/26"
  availability_zone  = "us-east-1a"


  tags = {
    Name = "ball-public-1"
  }
}

resource "aws_internet_gateway" "ball-igw" {
  vpc_id = aws_vpc.ball-pool-vpc.id

  tags = {
    Name = "ball-igw"
  }
}

resource "aws_route_table" "ball-rt" {
 vpc_id = aws_vpc.ball-pool-vpc.id
 
 route {
   cidr_block = "0.0.0.0/0"
   gateway_id = aws_internet_gateway.ball-igw.id
 }
 
 tags = {
   Name = "ball-rt"
 }
}

resource "aws_route_table_association" "public_association" {
 subnet_id      = aws_subnet.ball-public-1.id
 route_table_id = aws_route_table.ball-rt.id
}
